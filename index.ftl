<#--
	@package Akina
-->
<#include "header.ftl">
<@header title="${blog_title!}">
	<div class="blank"></div>
</@header>

<#if settings.head_notice!false && settings.notice_title!=''>
	<div class="notice" style="
	<#if (settings.focus_height!true)>
		margin-top: 20px;
	</#if>
		">
		<i class="iconfont icon-broadcast"></i>
		<div class="notice-content">${settings.notice_title!}</div>
	</div>
</#if>

<#if settings.top_feature!true>
	<#include "layouts/feature.ftl">
</#if>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<h1 class="main-title" style="font-family: 'Ubuntu', sans-serif;">
		<i class="fa fa-envira" aria-hidden="true"></i>
		<span class="i18n" data-iname="home.discovery"> </span>
		</h1>
		<#if posts?? && posts.getTotalElements() gt 0>
			<#--Start the Loop-->
			<#if (settings.post_list_style!'standard') == 'standard'>
				<#list posts.content as post>
					<#include "tpl/content.ftl">
				</#list>
			<#else>
				<#include "tpl/content-thumb.ftl">
			</#if>
		<#else>
			<#include "tpl/content-none.ftl">
		</#if>
	</main><!-- #main -->

	<@paginationTag method="index" page="${posts.number}" total="${posts.totalPages}" display="3">
		<#include "layouts/list-nextprev.ftl">
	</@paginationTag>
</div><!-- #primary -->

<#include "footer.ftl">
<@footer />

    <script src="https://www.wenjie.store/live2d/rain.js"></script>
    <script>
      Rain({
          speed: [10,25], // 风速范围
          hasBounce: true, // 是否有回弹效果
          wind_direction: 250, // 风向
          gravity: 0.163, // 重力
          maxNum: 50, // 雨滴最大数量
          numLevel: 5, // 每次生成雨滴数量
          drop_chance: 0.4 // 下雨的概率
      });
    </script>
